package com.gitlab.indicode.fabric.paperdoll;

import io.github.prospector.modmenu.api.ModMenuApi;
import java.util.function.Function;
import me.sargunvohra.mcmods.autoconfig1u.AutoConfig;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.gui.screen.Screen;

/**
 * @author Indigo A.
 */
@Environment(EnvType.CLIENT)
public class ModMenuIntegration implements ModMenuApi {
    public ModMenuIntegration() {

    }

    @Override
    public String getModId() {
        return "paperdoll";
    }

    @Override
    public Function<Screen, ? extends Screen> getConfigScreenFactory() {
        return (Function<Screen, Screen>) screen -> AutoConfig.getConfigScreen(Config.class, screen).get();
    }
}
