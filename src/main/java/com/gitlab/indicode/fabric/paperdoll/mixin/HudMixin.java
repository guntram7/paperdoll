package com.gitlab.indicode.fabric.paperdoll.mixin;

import com.gitlab.indicode.fabric.paperdoll.Config;
import com.gitlab.indicode.fabric.paperdoll.PaperDoll;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.hud.InGameHud;
import net.minecraft.client.gui.screen.ingame.InventoryScreen;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.math.MathHelper;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

/**
 * @author Indigo A.
 */
@Mixin(InGameHud.class)
public class HudMixin {
    @Inject(method = "render", at = @At("RETURN"))
    public void draw(MatrixStack stack, float float_1, CallbackInfo ci) {
        try {
            Config config = PaperDoll.config;
            PlayerEntity player = MinecraftClient.getInstance().player;
            if (config.only_activity && !(player.isSneaking() || player.isSprinting() || player.isSwimming() || player.handSwinging || player.isFallFlying())) return;
            //Remove Rotations
  /*
            float pitch = player.pitch, yaw = player.yaw, headYaw = player.headYaw;
            player.pitch = 0;
            player.setYaw(config.rotation);
            player.setHeadYaw(config.rotation);
            //Minify
*/            
            int scaleY = MathHelper.ceil(config.render_height / (config.dynamic_scale ? player.getHeight() : 2f));
            int scaleX = MathHelper.ceil(config.render_width / (config.dynamic_scale ? player.getWidth() : 1f));
            float scale = Math.min(scaleX, scaleY);
            //Position
            float glY = (float) config.y + (20 + config.render_height / 2f);
            float glX = (float) config.x + 20;
            
            InventoryScreen.drawEntity((int) glX, (int) glY, (int)scale, config.rotation, 0, player);
            
/*            
            //Draw
            GlStateManager.enableColorMaterial();
            GlStateManager.pushMatrix();

            GlStateManager.translatef(glX, glY - (config.change_swim_fly && (player.isSwimming() || player.isFallFlying()) ? config.render_height / 2f : 0), 50.0F);
            GlStateManager.scalef(-scale, scale, scale);
            GlStateManager.rotatef(180.0F, 0.0F, 0.0F, 1.0F);
            GlStateManager.rotatef(135.0F, 0.0F, 1.0F, 0.0F);
            GlStateManager.rotatef(-100.0F, 0.0F, 1.0F, 0.0F);
            GlStateManager.rotatef(0.0f, 1.0F, 0.0F, 0.0F);
            GuiLighting.enable();
            EntityRenderDispatcher rendermanager = MinecraftClient.getInstance().getEntityRenderManager();
            rendermanager.render(player, 0.0D, 0.0D, 0.0D, 0.0F, 1.0F, true);
            rendermanager.renderSecondPass(player, 0);

            GlStateManager.popMatrix();
            GuiLighting.disable();
            GlStateManager.disableRescaleNormal();
            GlStateManager.activeTexture(GLX.GL_TEXTURE1);
            GlStateManager.disableTexture();
            GlStateManager.activeTexture(GLX.GL_TEXTURE0);
            //Return rotations
            player.pitch = pitch;
            player.setYaw(yaw);
            player.setHeadYaw(headYaw);
*/
        } catch (Throwable ignored) {} // Why would I ever want to go find the *cause* of the issue...
    }
}
